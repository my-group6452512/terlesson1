/*output "instance_public_ips" {
  value       = aws_instance.instances[*].public_ip
  description = "Public IP addresses of the instances"
}*/

/*output "eip_allocation_ids" {
  value       = aws_eip.eips[*].id
  description = "Elastic IP allocation IDs"
}*/

output "aws_instance_public_dns" {
  value       = aws_instance.instance[*].public_dns
  description = "aws_instance_public_dns"
  }

output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.instance[*].id
}