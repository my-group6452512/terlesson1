variable "region" {
  type        = string
  default     = "us-east-1"
  description = "aws region"
}

variable "AWSZ" {
  type        = list (string)
  description = "AWS avaliable zone"
  default     = ["us-east-1a","us-east-1b", "us-east-1c"]
}

variable "subnet" {
  type        = list (string)
  description = "AWS internal subnets"
  default     = ["10.0.10.0/27", "10.0.20.0/27", "10.0.30.0/27"]
}

variable "instance_type" {
  description = "EC2 instance type"
  default     = "t2.micro"
}

variable "environments" {
  description = "List of environment names"
  type        = list(string)
  default     = ["test", "staging", "prod"]
}

variable "ssh_key_name" {
  description = "Name of the SSH key pair"
  type        = string
  default     = "HomeWkey"
}

variable "eip_allocation_ids" {
  type    = list(string)
  default = []
}

variable "vpc_cidr" {
  description = "CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "ami" {
  type        = string
  default     = "ami-053b0d53c279acc90"
  description = "Ubuntu Server Canonical 22.04 TLS"
}

