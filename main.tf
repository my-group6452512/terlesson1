resource "aws_vpc" "HomeWVPC" {
  cidr_block = var.vpc_cidr
  
  
  tags = {
    Name = "HomeW_VPC"
  }
}

resource "aws_internet_gateway" "HomeWG" {
  vpc_id = "${aws_vpc.HomeWVPC.id}"
}

resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.HomeWVPC.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.HomeWG.id}"
}

resource "aws_subnet" "HomeWSubnet" {
  count                   = length(var.AWSZ)
  vpc_id                  = "${aws_vpc.HomeWVPC.id}"
  cidr_block              = element(var.subnet, count.index)
  availability_zone       = element(var.AWSZ, count.index)
  map_public_ip_on_launch = true

  tags = {
    Name = "HomeW subnet"
  }
}

resource "aws_security_group" "HomeWSG" {
  vpc_id = aws_vpc.HomeWVPC.id
  name        = "HomeWSG"
  description = "allow 22,80,443  ports both direction"

  dynamic "ingress" {
    for_each = [22, 80, 443]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic "egress" {
    for_each = [22, 80, 443]
    content {
      from_port   = egress.value
      to_port     = egress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}

/*data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"]
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-22.04-amd64-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}*/

resource "aws_instance" "instance" {
  count                      = length(var.environments)
  subnet_id                   = aws_subnet.HomeWSubnet[count.index].id
  instance_type               = var.instance_type
  ami                         = var.ami
  vpc_security_group_ids      = [aws_security_group.HomeWSG.id]
  associate_public_ip_address = true
  key_name                    = aws_key_pair.HomeWkey.key_name

  tags = {
    Name = "${var.environments[count.index]}-instance-HomeW"
    
  }
}

#resource "aws_eip" "eips" {
# count    = length(var.subnet_cidrs)
# instance = aws_instance.instances[count.index].id
# vpc      = true
# }

#resource "aws_eip_association" "eip_assoc" {
# count         = length(var.subnet_cidrs)
# allocation_id = aws_eip.eips[count.index].id
# instance_id   = aws_instance.instances[count.index].id
# }


resource "tls_private_key" "rsa" {
   algorithm = "RSA"
   rsa_bits  = 4096
}

resource "aws_key_pair" "HomeWkey" {
  key_name   = var.ssh_key_name
  public_key = tls_private_key.rsa.public_key_openssh
}

resource "local_file" "HomeWkey" {
  content  = tls_private_key.rsa.private_key_pem
  filename = var.ssh_key_name
}